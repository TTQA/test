import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Parameter } from '../../model/request.model';
import { RequestService } from '../../services/request.service';

@Injectable({
  providedIn: 'root'
})
export class LoginFormService {

  public path = '/WebAPI/ValidateUser';

  constructor(
    private _request: RequestService,
  ) { }

  logIn(params: Parameter[]): Promise<any> {
    return firstValueFrom(this._request.getRecordAll(this.path, params));
  }

}
