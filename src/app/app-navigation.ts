export const navigation = [
  {
    text: 'Sales Order',
    path: '/SalesOrder',
    icon: 'home'
  },
  {
    text: 'Master Data',
    icon: 'folder',
    items: [
      {
        text: 'Customer',
        path: '/Customer'
      },
      {
        text: 'Product',
        path: '/Product'
      },
      {
        text: 'Store',
        path: '/Store'
      },
      {
        text: 'Payment',
        path: '/Payment'
      }
    ]
  }
];
